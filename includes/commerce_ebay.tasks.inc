<?php
/**
 *
 * @file commerce_ebay_tasks.inc
 *
 * @description
 *  a range of tasks that can be used by cron to
 *  synchronize products etc, or hooks that are
 *  called to keep things up to date.
 *
 */

/**
 *
 * @description
 *  syncronize all of the tasks currently queued, performing
 *  a different action per action queued. Requires that cron and
 *  hostname is enabled.
 *
 */
function commerce_ebay_task_sync_all_tasks() {
  $trading_service = new CommerceEbayTradingService();

  // only run if cron is enabled for host
  if (!$trading_service->cron_enabled() || !$trading_service->host_enabled()) {
    return;
  }

  $queue_limit = variable_get('ebay-queue-limit', 100);
  $queue = commerce_ebay_task_get_queue($queue_limit);

  // start from the last time it was synched
  $now = date('c', strtotime('now'));
  $last_sync = variable_get('ebay-last-sync', date('c', strtotime('now')));

  // increase execute timeout in case there are lots of items
  drupal_set_time_limit(2400);

  // 1. Add items
  if (isset($queue['add'])) {
    foreach ($queue['add'] as $idx => $nid) {
      $trading_service->addFixedPriceItem($nid);
      commerce_ebay_task_remove_from_queue($nid, 'add');
      unset($queue['add'][$idx]);
    }
  }

  // 2. Update local stock changes with Ebay transactions
  commerce_ebay_task_get_stock_changes($last_sync);

  // 3. Revise/End items
  if (isset($queue['revise'])) {
    foreach ($queue['revise'] as $idx => $nid) {
      $trading_service->reviseFixedPriceItem($nid);
      commerce_ebay_task_remove_from_queue($nid, 'revise');
      unset($queue['revise'][$idx]);
    }
  }

  // reset the last sync time to now
  variable_set('ebay-last-sync', $now);
}

/**
 *
 * @description
 *  get the stock changes from ebay items from a given start time and
 *  change the stock quantities of the local items accordingly.
 *
 * @param string $start_time (ISO8601)
 *
 */
function commerce_ebay_task_get_stock_changes($start_time=NULL) {
  // load this in because cron had been complaining it couldn't find the functions of commerce_ebay.tasks.inc
  module_load_include('inc', 'commerce_ebay', 'includes/commerce_ebay.tasks');

  $trading_service = new CommerceEbayTradingService();

  $start_time = ($start_time) ? $start_time : date('c', strtotime('-1 hour'));
  $end_time = date('c', strtotime('now'));

  // get all the items listed on the eBay store that have been modified.
  if ($listing = $trading_service->getSellerTransactions($start_time, $end_time)) {
    $items = array();

    // xml sometimes returns without a nested instance if there is only one instance
    // so it needs to be nested.
    if (isset($listing['TransactionArray']['Transaction']['AmountPaid'])) {
      $items[] = $listing['TransactionArray']['Transaction'];
    }
    if (isset($listing['TransactionArray']['Transaction'][0])) {
      $items = $listing['TransactionArray']['Transaction'];
    }

    foreach ($items as $item) {
      $variation_sku = $item['Variation']['SKU'];
      if ($product = commerce_product_load_by_sku($variation_sku)) {
        $mapping = new CommerceEbayMapping(NULL);
        // we now need to make sure we actually have a quantity field we can map to
        // so we load in the variation fields, find the Quantity field and set that
        // to the items Quantity value.
        $product_wrapper = entity_metadata_wrapper('commerce_product', $product);
        $quantity_field = isset($mapping->data['Variation Mapping'][$product->type]['Quantity'])
          ? $mapping->data['Variation Mapping'][$product->type]['Quantity'] : 'commerce_stock';

        // try catch block here in case an error occurs
        if (isset($product_wrapper->{$quantity_field})) {
          try {
            // we just need to do it for the amount purchased minus the local quantity
            $local_quantity = $product_wrapper->{$quantity_field}->value();
            $product_wrapper->{$quantity_field}->set($local_quantity - $item['QuantityPurchased']);
            $product_wrapper->save();
          }
          catch (Exception $e) {
            watchdog('commerce_ebay_tasks', 'Error updating stock for item: %s ',
              array('%s' => $item['Variation']['SKU']), WATCHDOG_INFO);
            continue;
          }
        }
      }
    }
  }
}

/**
 *
 * @description
 *  queues all products with a list flag to upload to ebay.
 *
 */
function commerce_ebay_task_queue_all_items() {

  // get a blank mapping so we have access to the YAML config
  if ($mapping = new CommerceEbayMapping(NULL)) {

    if (isset($mapping->data['Product Mapping'])) {
      // loop each product type defined in the mapping, get all of the nodes flagged
      // to be listed on ebay and then queue their nids
      foreach ($mapping->data['Product Mapping'] as $product_type=>$product_mapping) {
        if (isset($product_mapping['Add Fixed Price Item']['List On Ebay'])) {
          $list_flag = $product_mapping['Add Fixed Price Item']['List On Ebay'];
          $field_query = new EntityFieldQuery();
          try {
            $field_query->entityCondition('bundle', $product_type)
              ->fieldCondition($list_flag, 'value', 1);
            $result = $field_query->execute();
            if (isset($result['node'])) {
              foreach(array_keys($result['node']) as $nid) {
                commerce_ebay_task_queue_node($nid, 'add');
              }
            }
          }
          catch (Exception $exception) {
            watchdog('commerce_ebay_tasks', 'Error adding products of type: %type with list flag: %flag to node queue.',
              array('%type' => $product_type, '%flag' => $list_flag), WATCHDOG_INFO);
          }
        }
      }
    }
  }
  drupal_set_message(t('All products sent to sync queue'));
}

/**
 *
 * @description
 *  returns an organized array of queued nodes by action type, with an optional limit.
 *
 * @param $queue_limit
 *
 * @return array
 *
 */
function commerce_ebay_task_get_queue($queue_limit=NULL) {
  $queue = array();
  $query = db_select('commerce_ebay_product_sync_queue', 'q')->fields('q');

  if ($queue_limit) {
    $query->range(0, $queue_limit);
  }

  $data = $query->execute();

  while ($item = $data->fetchAssoc()) {
    $queue[$item['action']][] = $item = $item['nid'];
  }

  return $queue;
}

/**
 *
 * @description
 *  removes a task from the task queue.
 *
 * @param $nid
 *
 * @param null $action
 */
function commerce_ebay_task_remove_from_queue($nid, $action=NULL) {
  if ($nid && $action) {
    db_delete('commerce_ebay_product_sync_queue')
      ->condition('nid', $nid, '=')
      ->condition('action', $action, '=')
      ->execute();
  }
}

/**
 *
 * @description
 *  adds a node id and action type to the product sync queue,
 *  checks for duplicates before uploading.
 *
 * @param $nid
 * @param null $action
 *
 * @throws Exception
 *
 */
function commerce_ebay_task_queue_node($nid, $action=NULL) {
  if ($nid && $action) {
    db_merge('commerce_ebay_product_sync_queue')
        ->key(array('nid' => $nid))
        ->fields(array(
            'nid' => $nid, 'action' => $action
        ))
        ->execute();
  }
}
