<?php
/**
 *
 * @file commerce_ebay.constants.inc
 *
 */

define("XML_SANDBOX_GATEWAY_URI", "https://api.sandbox.ebay.com/ws/api.dll");
define("SOAP_SANDBOX_GATEWAY_URI", "https://api.sandbox.ebay.com/wsapi");

define("XML_PRODUCTION_GATEWAY_URI", "https://api.ebay.com/ws/api.dll");
define("SOAP_PRODUCTION_GATEWAY_URI", "https://api.ebay.com/wsapi");
