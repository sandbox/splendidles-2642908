CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Troubleshooting
  * Maintainers


INTRODUCTION
------------
This module facilitates the listing of Drupal Commerce products on Ebay, and automated stock management. It uses a
YAML config file, to define a feed which is then transposed by a Mapping class, which fills in all of the values with
either plain text, node data or product variation data. A sample file is provided with a production working example
which can then be tailored to suit any extra needs.

  * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/splendidles/2642908


REQUIREMENTS
------------
This module requires the following modules:

  * Drupal Commerce (https://www.drupal.org/project/commerce)
  * Elysia Cron (https://www.drupal.org/project/elysia_cron)


INSTALLATION
------------

  * SPYC YAML (https://github.com/mustangostang/spyc) must be cloned from github to the following directory
   within the module: libraries/mustangostang/spyc. This can be done manually, or automatically via composer.
   Once installed the Spyc.php file must be accessible in the following structure:
   commerce_ebay/libraries/mustangostang/spyc/Spyc.php

  * Array2XML (https://github.com/nonapod/Array2XML) must be cloned from the commerce-ebay branch to the
    following directory within the module: libraries/nonapod/Array2XML. This can be done manually, or automatically
    via composer. Once installed the Array2XML.inc file must be accessible in the following structure:
    commerce_ebay/libraries/nonapod/Array2XML/Array2XML.inc
    @NOTE this must be cloned from the commerce-ebay branch, master will no work. Composer will take care of this.

  * Next Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

  * (recommended) If you haven't already set up an eBay sandbox, it is highly recommended that you do, so you can confirm
    the feed is working first and get a feed that you're happy with. Go to the following URLs for more information on
    creating an eBay sandbox and an eBay developer account:
    - https://developer.ebay.com
    - https://go.developer.ebay.com/developer-sandbox
    - https://developer.ebay.com/devzone/guides/ebayfeatures/Basics/Call-SandboxTesting.html

  * Add a boolean field to your product display nodes. These being the nodes that hold the product variations.
    The field should be a flag called something like List On Ebay. Then set items that you wish to list on
    eBay with this flag

  * Add the following permission 'administer commerce ebay'  to those who should be allowed to administer
    the eBay settings via the admin panel

  * Go to the admin page admin/commerce/config/ebay in here you can configure the following settings
    - Sandbox Mode: You should have this on while in development, it will sync your items to your sandbox store instead
      of your live store.
    - Enable Cron: If this is turned on, the regular synchronisation of stock and item listings will be enabled, you
      will probably want to leave this off until you have a workable feed.
    - Queue Limit: This is how many items will be sent to eBay per cron run, this prevents a large stream being sent
      to eBay which may upset the api usage limit or performance. This defaults to 100.
    - Restrict Cron To Hostname: This must be the hostname of the server the app is running on, this is checked against
      the result of the function gethostname(), so the physical hostname of the server. This prevents any accidental
      cron synchronisation when developing locally on a recent copy of the database.
    - Config Filename: The name of the YAML config file, it must be placed inside the includes directory of the module,
      this defaults to commerce_ebay.map.yaml
    - Sandbox Settings: You should have a sandbox for testing, you will need the following settings from your ebay
      developer account: Dev ID, App ID, Cert ID, Token and RuName. The Site ID should be the country the store is in,
      which can be selected from a drop down. Check the ebay developers site for instructions on how to get these
      values: http://developer.ebay.com/devzone/xml/docs/howto/tokens/gettingtokens.html
    - Production Settings: The Production Settings use the same settings as the Sandbox Settings, only the production
      variants.
    - Sample Feed Generator: This is the perfect tool for seeing what your feed will look like once configured and how
      it will be transmitted to eBay. The functions you'll most likely want to test are Add Fixed Price Item and
      Revise Fixed Price Item. Both of these functions work by providing a node nid for a product display node, it will
      then transpose the YAML feed to the appropriate XML which you can look over for any errors you may be getting from
      eBay. In the instance where a feed argument is called "skus", then you must provide a comma delimited list of
      product skus.

  * Copy the commerce_ebay.map.yaml.sample file to a new file called commerce_ebay.map.yaml in the includes directory,
    you can choose a different name for the file if you wish but make sure you set this name in the admin settings as
    mentioned above.

  * The yaml has been commented quite meticulously, using the commenting as a guideline, modify the feed to suit your
    needs. The following is a quick explanation of how the yaml is constructed:

        - The first thing you'll find in the yaml sample is an entry at the top that looks like the following:

          Variation Mapping:
            product:
              Quantity: commerce_stock

          this serves as a simple access to the product variation type Quantity field, which is required for some
          tasks and prevents the need to descend into a convoluted mapping. Note that the only purpose is for access
          to the quantity field at the moment, the real variation mapping is done within each individual feed for each
          display node.

        - The next set of mapped data is for the feeds themselves, which will look similar to the following:

          Product Mapping:
            node_type:
              Add Fixed Price Item:
                ...
              Revise Fixed Price Item:
                ...

          in this case "node_type" refers to the node type you're using as your main product node that contains the
          product variations, in the sample yaml file this is called "product_display". Inside the config you will
          see some fields defined as plain text, these will remain as plain text. You will see some fields have a
          prefix and some extra config appended to them, the following is a description of these:

            - Plain Text fields will be transposed as simply plain text.
            - Fields that are prefixed/suffixed with % symbols i.e. %field_quantity% will take the value from the
              parent/display node that holds the variations provided that field exists.
            - Fields that are prefixed/suffixed with the $ symbols i.e. $field_sku$ will take the value from the
              current variation in question if in the "Variation Fields", "Variation Specifics" or "Variation Pictures"
              portion of the config. Which are required for the feed to work. If this prefix/suffix is present outside
              of these segments, the value will come from the first variation in the display/parent node.
            - Fields that have been defined with a nested value i.e $field_product_dimensions$->weight, will take
              the value from the variation (because of $ prefix/suffix) field, and then return the weight value of
              that fields returned object/array as the field_product_dimensions field could also contain weight,
              height, length etc.
            - Fields that ends with a pipe character and a function will run the return value through that function
              if the variable $context, has been provided to it. i.e.
              $field_product_dimensions$->width|number_format($context, 2) this example will take the variation field
              (because of the prefix/suffix $ symbols) and the width value within the returned object because of the
              ->width at the end, and then pass it to the number_format function with the field width value passed in
              as the context because $context has been passed in as a variable, the second variable is a 2. The result
              of this will be the fields with value passed to the number_format function to 2 decimal places.
                Note that using a function should be kept very simplistic, and every value passed to the function in
              assumed to be a string, so no arrays should be passed as this will break it.

        - Inside the individual feeds "Add Fixed Price Item" and "Revise Fixed Price Item", you will find definitions
          similar to these below:

          Variation Fields:
            SKU: $sku$
            ...

          Variation Specifics:
            NameValueList:
              -
                Name: Size
                Value: $field_size$
              ...

          Variation Pictures:
            Specific Name: Color
            ...

          these MUST be mapped in order to make the feed work, products that do not have variations won't work. The
          first of these are the Variation Fields themselves, these are the fields each individual variation will have
          when the feed is transposed, as the variations are looped.
            Next are the Variation Specifics, the example above contains Size, you may want to follow eBays guidelines
          for variations and their specifics:
          https://developer.ebay.com/devzone/guides/ebayfeatures/Development/Variations-Configuring.html
            Finally you must map the variation pictures, using one variation specific identifier to differentiate the
          picture i.e. if your pictures differ in color, then use the Color as the specific name etc.


        - Categories are mapped at the bottom of the file as followed:

          Category Mapping:
            $default: 16592
            Some Category Name: 15678
            ...

          Store Category Mapping:
            $default: 0
            Some Category Name: 15678
            ...

          Category refers to the category on ebay itself, not on your ebay store which are separate and must be created
          manually. $default refers to the default category your items will go to, everything beneath that will be
          matched to the items category name, if the items category name matches whatever is mapped in the Category
          Mapping, it will use that category ID. The same also goes for the Store Category Mapping, if a category isn't
          recognized in the Store Category Mapping, it will default to the "Other" category.
            Note that the category mapping is fairly simple at the moment, it doesn't take into account the fact
          some sub-categories may share the same name as other sub-categories etc, simply if the name matches what is
          found in the config, it will be set to that ID. This will be improved upon in the future.


    * Once you have a feed configured, you should examine the XML using the Sample Feed Generator in the admin
      config: admin/commerce/config/ebay
        Expand the Sample Feed Generator section and then select Add Fixed Price Item as the feed, in the feed
      arguments provide a node id for a product that you have set to list on ebay. This will only work if that
      product has been set to be listed. Hit sumbit and you should see some sample XML come through, note this will
      not push anything live.

    * If you're comfortable with the feed, you should enable the cron in sandbox mode and hit Queue All Items and then
      run the cron, this will upload the queued items to eBay in a batch based off of the Queue Limit. You can then
      check your logs and your sandbox store, to see how it turned out. You should do a test sale or two and adjust some
      stock and run the cron a few times just to make sure everything is working smoothly. When you're comfortable,
      uncheck the sandbox mode option in the admin settings and hit Queue All Items again and let the cron job
      get to work. You should begin to see your eBay store populate with items if all goes well.


TROUBLESHOOTING
---------------

- Have you run through the steps mentioned above in the Configuration segment of the Readme file, and have you
  copied the yaml template and created your own version from it? If not please read the Configuration segment.
- When troubleshooting or developing your feed for the first time, you should be using the Sample Feed Generator frequently
  to make sure the XML is forming correctly, and that your feed complies with eBay requirements.
- Always check the logs, should the eBay service report an error, it will be logged. One of the most annoying things
  you will find, will be working with the eBay API, it can report a lot of hard to decipher errors and this will usually
  boil down to a field that has been mapped in correctly or isn't formatted the way eBay expects it.
- Use the sandbox mode whenever developing.
- Double check your Sandbox and Production settings to make sure these match what you generated on the eBay Developer
  site.
- Use the following tool to interact directly with the eBay API to confirm that your solution works there first before
  thinking the error might be with the module: https://developer.ebay.com/DevZone/build-test/test-tool

MAINTAINERS
-----------

Current maintainers:
  * Les Cordell (splendidles) - https://www.drupal.org/u/splendidles

This project has been sponsored by:
  * Acro Media Inc. - https://www.acromediainc.com/
