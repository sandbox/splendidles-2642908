<?php

/**
 *
 * @file commerce_ebay.Service.inc
 *
 * @description
 *  sets up a core service call for the ebay API
 *
 */
class CommerceEbayService {

  protected $version;
  protected $callname;
  protected $api_endpoint;
  protected $token_expiry;
  protected $current_token;
  protected $session;
  protected $service_name;
  public    $test_run;

  protected $xml;
  protected $curl_connection;
  protected $xml_request;

  function __construct() {
    $this->version = 935;
    $this->service_name = "Ebay_Core_Service";
    $this->callname = '';
    $this->test_run = FALSE;
    $this->xml = new CommerceEbayXML();
    $this->setAPIEndpoint();
  }

  /**
   *
   * @description
   *  return the globalID
   *
   * @return string|null
   *
   */
  function globalId() {
    $siteID = $this->siteID();
    foreach (_commerce_ebay_global_mapping() as $mapping) {
      if ($mapping['Site ID'] == $siteID) {
        return $mapping['Global ID'];
      }
    }
    return NULL;
  }

  /**
   *
   * @description
   *  setter for the api endpoint; provide an
   *  endpoint uri exclusively, or use production/
   *  sandbox uris'.
   *
   * @param String $endpoint
   *
   */
  function setAPIEndpoint($endpoint='') {

    if (empty($endpoint)) {

      if ($this->debug()) {
        $this->api_endpoint = XML_SANDBOX_GATEWAY_URI;
      }

      else {
        $this->api_endpoint = XML_PRODUCTION_GATEWAY_URI;
      }

    }

    else {
      $this->api_endpoint = $endpoint;
    }

  }

  /**
   *
   * @description
   *  return debug mode setting
   *
   * @return Bool
   *
   */
  public static function debug() {
    return variable_get('ebay-sandbox', 0);
  }


  /**
   *
   * @description
   *  getter: Enable Cron
   *
   * @return bool
   *
   */
  public function cron_enabled() {
    return variable_get('ebay-enable-cron', 0);
  }

  /**
   *
   * @description
   *  getter: Enable Host Cron
   *
   * @return bool
   *
   */
  public function host_enabled() {
    return variable_get('ebay-host-restrict', '') == gethostname();
  }

  /**
   *
   * @description
   *  return production or sandbox appID
   *
   * @return String
   *
   */
  function appID() {
    if ($this->debug()) {
      return variable_get('ebay-sandbox-appid', '');
    }
    return variable_get('ebay-production-appid', '');
  }

  /**
   *
   * @description
   *  return production or sandbox devID
   *
   * @return String
   *
   */
  function devID() {
    if ($this->debug()) {
      return variable_get('ebay-sandbox-devid', '');
    }
    return variable_get('ebay-production-devid', '');
  }

  /**
   *
   * @description
   *  return production or sandbox siteID
   *
   * @return String
   *
   */
  function siteID() {
    if ($this->debug()) {
      return variable_get('ebay-sandbox-siteid', '');
    }
    return variable_get('ebay-production-siteid', '');
  }

  /**
   *
   * @description
   *  return production or sandbox certID
   *
   * @return String
   *
   */
  function certID() {
    if ($this->debug()) {
      return variable_get('ebay-sandbox-certid', '');
    }
    return variable_get('ebay-production-certid', '');
  }

  /**
   *
   * @description
   *  return production or sandbox RuName
   *
   * @return String
   *
   */
  function ruName() {
    if ($this->debug()) {
      return variable_get('ebay-sandbox-runame', '');
    }
    return variable_get('ebay-production-runame', '');
  }

  /**
   *
   * @description
   *  construct the ebay curl call; uses instance
   *  endpoint, headers and xml request.
   *
   */
  private function buildRequest() {
    $this->curl_connection = curl_init();
    curl_setopt($this->curl_connection, CURLOPT_URL, $this->api_endpoint);
    curl_setopt($this->curl_connection, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($this->curl_connection, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($this->curl_connection, CURLOPT_HTTPHEADER, $this->headers());
    curl_setopt($this->curl_connection, CURLOPT_POST, 1);
    curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $this->xml_request);
    curl_setopt($this->curl_connection, CURLOPT_RETURNTRANSFER, 1);
  }

  /**
   *
   * @description
   *  run the actual service, returning a decoded XML response
   *  as an array of values.
   *
   * @return mixed
   *
   */
  protected function runService() {
    $this->buildRequest();
    $result = curl_exec($this->curl_connection);
    curl_close($this->curl_connection);
    $response = $this->xml->readXML($result);

    // return the decoded xml response
    return $this->decodeResponse($response);
  }

  /**
   *
   * @description
   *  return required call headers
   *
   * @return array
   *
   */
  protected function headers() {
    return array(
        "X-EBAY-API-COMPATIBILITY-LEVEL:" . $this->version,
        "X-EBAY-API-DEV-NAME:" . $this->devID(),
        "X-EBAY-API-APP-NAME:" . $this->appID(),
        "X-EBAY-API-CERT-NAME:" . $this->certID(),
        "X-EBAY-API-SITEID:" . $this->siteID(),
        "X-EBAY-API-CALL-NAME:" . $this->callname,
    );
  }

  /**
   *
   * @description
   *  convert a SimpleXMLDocument in its entirety, to array,
   *  this will lose any attributes the document might have.
   *
   * @param $response
   *
   * @return mixed
   *
   */
  protected function decodeResponse($response) {
    return json_decode(json_encode((array)$response), TRUE);
  }

  /**
   *
   * @description
   *  return production or sandbox token
   *
   * @return String
   *
   */
  function token() {
    if ($this->debug()) {
      return variable_get('ebay-sandbox-token', '');
    }
    return variable_get('ebay-production-token', '');
  }

  /**
   *
   * @description
   *  a little helper function to log an error from a set of call results
   *
   * @param null $result
   *
   */
  protected function logResultError($result=NULL, $code=WATCHDOG_ERROR) {
    if ($result) {
      if (isset($result['Errors'][0])) {
        foreach ($result['Errors'] as $error) {
          watchdog('Ebay_Trading', str_replace(array('<', '>'), '"', $error['LongMessage']), array(), $code);
        }
      }
      else {
        watchdog('Ebay_Trading', str_replace(array('<', '>'), '"', $result['Errors']['LongMessage']), array(), $code);
      }
    }
  }
}