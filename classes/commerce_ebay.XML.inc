<?php
/**
 *
 * @file commerce_ebay.XML.inc
 *
 * @description
 *  handle all of the XML serialization and parsing,
 *  uses SimpleXML.
 *
 */

class CommerceEbayXML {

  public $data;

  function __construct() {
    $this->data = array();
  }

  /**
   *
   * @description
   *  converts service data into XML
   *
   * @param $request_name
   *
   * @return string
   *
   */
  function getXML($request_name) {
    $xml = CommerceEbayArray2XML::createXML($request_name, $this->data);
    return $xml->saveXML();
  }

  /**
   *
   * @description
   *  load in xml from a string
   *
   * @param string $xml
   *
   * @return array|SimpleXMLElement|string
   *
   */
  function readXML($xml="") {
    if (is_string($xml) && $xml = simplexml_load_string($xml)) {
      return $xml;
    }
    return array();
  }



}