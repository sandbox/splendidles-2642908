<?php
/**
 *
 * @file commerce_ebay.Mapping.inc
 *
 * @description
 *  a class that holds information about a display node to product type mapping
 *
 */

class CommerceEbayMapping {

  public $node;
  public $data;
  public $feed_type;

  function __construct($node_or_nid) {
    if (gettype($node_or_nid) == 'string') {
      $node_or_nid = node_load($node_or_nid);
    }
    $this->node = entity_metadata_wrapper("node", $node_or_nid);
    $this->feed_type = "";
    // load the mapping form
    $module_path = drupal_get_path('module', 'commerce_ebay');
    require_once("$module_path/libraries/mustangostang/spyc/Spyc.php");
    $map_file_path = variable_get('ebay-config-filename', 'commerce_ebay.map.yaml');
    $this->data = Spyc::YAMLLoad("$module_path/includes/$map_file_path");
  }

  /**
   * @description
   *  a helper function to see if the node mapped to this instance is
   *  set to be listed on ebay or not.
   *
   * @param string $feed_name
   *
   * @return boolean
   *
   */
  public function listOnEbay($feed_name='Add Fixed Price Item') {
    if (!empty($this->node) && isset($this->data['Product Mapping'][$this->node->getBundle()]
        [$feed_name]['List On Ebay'])) {
      $list_field = $this->data['Product Mapping'][$this->node->getBundle()][$feed_name]['List On Ebay'];
      if (isset($this->node->{$list_field})) {
        return $this->node->{$list_field}->value();
      }
    }
    return FALSE;
  }


  /**
   *
   * @description
   *  an assumption making display field fetcher, this function assumes that the Display Fields
   *  section has been mapped correctly for the loaded node in the instance, this function was
   *  made purely to get the value for the SKU entry which is required for mapping.
   *
   * @param $field_key
   * @param string $feed_name
   *
   * @return array|null|string
   *
   */
  public function getDisplayField($field_key, $feed_name='Add Fixed Price Item') {
    if (!empty($this->node) && isset($this->data['Product Mapping'][$this->node->getBundle()]
        [$feed_name]['Display Fields'][$field_key])) {
      $field_name = $this->data['Product Mapping'][$this->node->getBundle()][$feed_name]['Display Fields'][$field_key];
      return $this->_getFieldKeyVal($field_key, $field_name);
    }
    return NULL;
  }


  /**
   *
   * @description
   *  parses the commerce_ebay.map.yaml file and transposes it into
   *  data, a feed type must be passed as an argument.
   *
   * @param $feed_type
   *
   * @return array
   *
   */
  public function transpose($feed_type) {
    $this->feed_type = $feed_type;
    $data = array();
    $context = NULL;

    if (isset($this->data['Product Mapping'][$this->node->getBundle()][$this->feed_type])) {
      $context = $this->data['Product Mapping'][$this->node->getBundle()][$this->feed_type];
    }

    if ($context) {
      if (isset ($this->node->{$context['List On Ebay']}) && $this->node->{$context['List On Ebay']}->value()) {
        // transpose the parent/display node fields
        foreach ($context['Display Fields'] as $display_field_key => $display_field_val) {
          $value = $this->_getFieldKeyVal($display_field_key, $display_field_val);
          if (!empty($value) || $value === '0' || $value ===  0 ) {
            $data[$display_field_key] = $this->_getFieldKeyVal($display_field_key, $display_field_val);
          }
        }
        // transpose the variation fields
        if (isset($this->node->{$context['Product Variation']})) {
          $all_specifics = array(); // these will be added back to the top level VariationSpecificsSet
          if (isset($context['Variation Pictures']['Specific Name'])) {
            $data['Variations']['Pictures']['VariationSpecificName'] = $context['Variation Pictures']['Specific Name'];
          }
          foreach ($this->node->$context['Product Variation']->value() as $variation_idx => $variation) {
            $variation = entity_metadata_wrapper('commerce_product', $variation);
            // add the variation fields per variation
            foreach ($context['Variation Fields'] as $variation_field_key => $variation_field_val) {
              $value = $this->_getFieldKeyVal($variation_field_key, $variation_field_val, $variation);
              if (!empty($value) || $value === '0' || $value === 0) {
                $data['Variations']['Variation'][$variation_idx][$variation_field_key] = $value;
              }
            }
            // add the variation specifics per variation
            foreach ($context['Variation Specifics'] as $specific_field_key => $specific_field_val) {
              $specific_content = $this->_getFieldKeyVal($specific_field_key, $specific_field_val, $variation);
              if ($specific_content) {
                $value = $this->_getFieldKeyVal($specific_field_key, $specific_field_val, $variation);
                if (!empty($value) || $value === '0' || $value === 0) {
                  $data['Variations']['Variation'][$variation_idx]['VariationSpecifics'][$specific_field_key] = $value;
                }
              }
            }
            if (isset($data['Variations']['Variation'][$variation_idx]['VariationSpecifics']['NameValueList'])) {
              // remove any variation specifics without values
              foreach ($data['Variations']['Variation'][$variation_idx]['VariationSpecifics']['NameValueList']
                      as $specific_idx => $specific) {
                if (!isset($specific['Value']) || !$specific['Value']) {
                  unset($data['Variations']['Variation'][$variation_idx]['VariationSpecifics']
                    ['NameValueList'][$specific_idx]);
                }
                else {
                  $all_specifics[$specific['Name']][] = $specific['Value'];
                  // add the specifics to a VariationSpecificsSet at the top level of the
                  // variations: Item.Variations.VariationSpecificsSet
                }
                // add the variation pictures to the top level of variations if we have a
                // Variation Pictures field definition.
                if (isset($context['Variation Pictures']['Field'])
                  && $specific['Name'] == $context['Variation Pictures']['Specific Name']) {
                    $pictures = array();
                    $image_urls = $this->_getFieldKeyVal('Pictures', $context['Variation Pictures']['Field'], $variation);
                    foreach ($image_urls as $image_url) {
                      $pictures[] = $image_url;
                    }
                    $data['Variations']['Pictures']['VariationSpecificPictureSet'][] = array(
                      'PictureURL' => $pictures,
                      'VariationSpecificValue' => $specific['Value']
                    );
                }
              }
            }
          }
          foreach (array_keys($all_specifics) as $name) {
            $data['Variations']['VariationSpecificsSet']['NameValueList'][] = array(
              'Name' => $name,
              'Value' => $all_specifics[$name],
            );
          }
        }
      }
    }
    return $data;
  }

  /**
   *
   * @description
   * filters the field values prefix/suffix and recursively deals with nested
   * instances, the fields are passed to the _getFieldKeyVal function with the
   * correct context and nested field values if present.
   *
   * @param $field_key
   * @param $field_val
   * @param bool $variation
   *
   * @return array|string
   *
   */
  protected function _getFieldKeyVal($field_key, $field_val, $variation=False) {
    $no_match = TRUE;
    $val_is_array = gettype($field_val) == 'array';
    $data = "";
    $nested_field_values = array(); // these are the specific values within the field i.e. $dimensions$->length
    $filter_function = NULL;

    if ($val_is_array) {
      foreach ($field_val as $nested_key => $nested_val) {
        $data[$nested_key] = $this->_getFieldKeyVal($nested_key, $nested_val, $variation);
      }
    }
    else {

      // get the filter function if there is one
      preg_match('/\|(.+)/i', $field_val, $matches);
      if (isset($matches[1])) {
        $filter_function = $matches[1];
        // remove the filter function from the field value
        $field_val = str_replace("|$filter_function", '', $field_val);
      }

      // get any nested values if there are any
      preg_match('/^[\$\%].+[\$\%](\-\>.+)/i', $field_val, $matches);
      if (isset($matches[1])) {
        $nested_field_values = array_filter(explode('->', $matches[1]));
      }

      // strip out the field_names prefix/suffix
      // match node fields
      preg_match('/\%(.+|.+\-\>)\%/', $field_val, $matches);
      if (isset($matches[1])) {
        if (isset($this->node->{$matches[1]})) {
          $field_name = $matches[1];
          if (!empty($nested_field_values)) {
            $data = $this->_getNestedFieldVal($field_name, $this->node, $nested_field_values);
          }
          else {
            $data = $this->_getFieldVal($field_name, $this->node, $field_key);
          }
        }
        // on data and filter function, pass it through the function
        if ($data && $filter_function) {
          $filter_function = str_replace('$context', $data, $filter_function);
          $data = $this->_execFilterFunction($filter_function, $data);
        }
        $no_match = FALSE;
      }

      // match variation fields
      preg_match('/\$(.+|.+\-\>)\$/', $field_val, $matches);
      if (isset($matches[1])) {
        // we need a variation to use the $ prefix/suffix. However if none is available
        // we assume that the first variation is intended.
        if (!$variation) {
          $context = '';
          if (isset($this->data['Product Mapping'][$this->node->getBundle()][$this->feed_type])) {
            $context = $this->data['Product Mapping'][$this->node->getBundle()][$this->feed_type];
          }
          $variations = $this->node->$context['Product Variation']->value();
          if (isset($variations[0])) {
            $variation = entity_metadata_wrapper('commerce_product', $variations[0]);
          }
        }
        if (isset($variation->{$matches[1]})) {
          $field_name = $matches[1];
          if (!empty($nested_field_values)) {
            $data = $this->_getNestedFieldVal($field_name, $variation, $nested_field_values);
          }
          else {
            $data = $this->_getFieldVal($field_name, $variation, $field_key);
          }
        }
        // on data and filter function, pass it through the function
        if ($data || $data === '0' || $data === 0 && $filter_function) {
          $filter_function = str_replace('$context', $data, $filter_function);
          $data = $this->_execFilterFunction($filter_function, $data);
        }
        $no_match = FALSE;
      }

      if ($no_match && !$val_is_array) {
        $data = $field_val;
      }
    }

    return $data;
  }


  /**
   *
   * @description
   *  helper function to handle passing values through a function, in
   *  the mapping the following format can be set:
   *  $field_name$|function($context, args...)
   *
   * @param $filter_function
   * @param string $data
   *  the original data can be passed and returned if necessary.
   *
   * @return mixed|string
   *
   */
  function _execFilterFunction($filter_function, $data="") {
    $args = array();
    $function = "";
    // get the function name
    preg_match('/^(.+)\(/i', $filter_function, $matches);
    if (isset($matches[1]) && function_exists($matches[1])) {
      $function = $matches[1];
    }
    // get the function args
    preg_match('/\((.+)\)/i', $filter_function, $matches);
    if (isset($matches[1])) {
      $args = explode(',', $matches[1]);
    }

    if (function_exists($function)) {
      $data = call_user_func_array($function, $args);
    }

    return $data;
  }


  /**
   *
   * @description
   *  gets the actual field value from a context entity_wrapper i.e
   *  node, product variation, a switch statement returns the most
   *  desired value from the fields.
   *
   * @param $field_name
   * @param $context_wrapper
   * @param bool $field_key
   *
   * @return array|null|string
   *
   */
  protected function _getFieldVal($field_name, $context_wrapper, $field_key=FALSE) {

    if (isset($context_wrapper->{$field_name})) {
      $field_type = field_info_field($field_name);

      switch ($field_type['type']) {

        case 'physical_dimensions':
          $dimensions = $context_wrapper->{$field_name}->value();
          return $dimensions;

        case 'text_with_summary':
          // we want to get the regular value for the text instead
          // of the safe value so we need to make sure it's not empty first
          $value = $context_wrapper->{$field_name}->value();
          if (isset($value['value'])) {
            // the description field cannot have HTML, so we need to strip it of tags.
            return $context_wrapper->{$field_name}->value->value();
          }
          return "";

        case 'taxonomy_term_reference':
          // Override here if we're dealing with a PrimaryCategory, SecondaryCategory or StoreCategoryID
          $term = $context_wrapper->{$field_name}->value();
          $term = (is_array($term)) ? $term[0] : $term;
          // Category Mapping in the yaml file
          if (in_array($field_key, array('CategoryID'))) {
            if (isset($this->data['Category Mapping'][$term->name])) {
              return $this->data['Category Mapping'][$term->name];
            }
            elseif (isset($this->data['Category Mapping']['$default'])) {
              return $this->data['Category Mapping']['$default'];
            }
            return "";
          }
          // Story Category Mapping in the yaml file
          if (in_array($field_key, array('StoreCategoryID'))) {
            if (isset($this->data['Store Category Mapping'][$term->name])) {
              return $this->data['Store Category Mapping'][$term->name];
            }
            elseif (isset($this->data['Store Category Mapping']['$default'])) {
              return $this->data['Store Category Mapping']['$default'];
            }
            return "";
          }
          return ($term) ? $term->name : NULL;

        case 'physical_weight':
          $weight = $context_wrapper->{$field_name}->value();
          return $weight;

        case 'commerce_price':
          $currency = $context_wrapper->{$field_name}->value();
          return commerce_currency_amount_to_decimal($currency['amount'], $currency['currency_code']);

        case 'image':
          $images = $context_wrapper->{$field_name}->value();
          $urls = array();
          foreach ($images as $image) {
            $urls[] = file_create_url($image['uri']);
          }
          return $urls;

        default:
          return $context_wrapper->{$field_name}->value();
      }
    }
    return "";
  }

  /**
   *
   * @description
   *  this function will attempt to drill into a field value, for instance
   *  product dimensions have 4 values inside their returned array value
   *  so when a field is detected when transposing with a similar format:
   *  $field_machine_name$->value or %field_machine_name%->value it will
   *  drill into the field to get the values within it.
   *
   * @param $field_name the field machine name
   * @param $context an entity_wrapper
   * @param $nested_field_values an array of nested values
   *
   * @return string
   *
   */
  protected function _getNestedFieldVal($field_name, $context, $nested_field_values) {
    $data = "";
    if (isset($context->{$field_name})) {
      $data = $context->{$field_name}->value();
      foreach ($nested_field_values as $val) {
        if (isset($data->{$val})) {
          $data = $data->{$val};
        }
        elseif (isset($data[$val])) {
          $data = $data[$val];
        }
        else {
          $data = "";
          break;
        }
      }
    }

    return $data;
  }
}

