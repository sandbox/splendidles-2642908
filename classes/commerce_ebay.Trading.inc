<?php

/**
 *
 * @file commerce_ebay.Trading.inc
 *
 * @description
 *  implements the Ebay Trading API, using CommerceEbayService as parent
 *
 */

module_load_include('inc', 'commerce_ebay', 'includes/commerce_ebay.constants');

class CommerceEbayTradingService extends CommerceEbayService {

  protected $sessionID;
  protected $tokenActive;
  protected $tokenExpiry;
  protected $service_name;

  function __construct() {
    parent::__construct();
    $this->getSessionID();
    $this->getTokenStatus();
    $this->service_name = "Ebay_Trading";

    // @TODO hold off on this for now, a regular token might do the trick rather than auth & auth.
    // Generate a new token if it's expired
    // $this->fetchToken();
  }

  /* ----------- API CALLS ----------- */

  /**
   *
   * @description
   *   Check to see if the token has expired or not.
   *  !Note that this isn't being utilised in the current version but it kept here in
   *        case of future additions or in case the class is used elsewhere.
   *
   * @return boolean
   *
   */
  function hasTokenExpired() {
    if (!$this->tokenActive && date(DATE_ISO8601, time()) > $this->tokenExpiry) {
      return True;
    }
    return False;
  }

  /**
   *
   * @description
   *  This function gets the current token status as defined in the api:
   *  http://developer.ebay.com/DevZone/XML/docs/Reference/ebay/GetTokenStatus.html
   *  !Note that this isn't being utilised in the current version but it kept here in
   *        case of future additions or in case the class is used elsewhere.
   *
   * @return mixed: XML string if test run or object result if live run
   *
   */
  function getTokenStatus() {
    $this->tokenActive = False;
    $this->tokenExpiry = 0;

    $this->callname = 'GetTokenStatus';
    $this->xml->data = array(
        "@attributes" => array(
            "xmlns" => "urn:ebay:apis:eBLBaseComponents"
        ),
        "RequesterCredentials" => array(
            "eBayAuthToken" => $this->token(),
        ),
    );

    $this->xml_request = $this->xml->getXML($this->callname);


    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    $result = $this->runService();

    if ($result['Ack'] == 'Success') {
      ($result['TokenStatus']['Status'] == 'Active') ? $this->tokenActive = True : $this->tokenActive = False;
      $this->tokenExpiry = $result['TokenStatus']['ExpirationTime'];
    }

    return $result;
  }

  /**
   *
   * @description
   *  This function delete a store categores
   *  http://developer.ebay.com/devzone/xml/docs/Reference/ebay/SetStoreCategories.html
   *  !Note that this isn't being utilised in the current version but it kept here in
   *        case of future additions or in case the class is used elsewhere.
   *
   * @return mixed: XML string if test run or boolean on success/failure
   *
   */
  function deleteStoreCategories($categories, $parent_id=NULL) {
    $this->tokenActive = False;
    $this->tokenExpiry = 0;

    $this->callname = 'SetStoreCategories';
    $this->xml->data = array(
        "@attributes" => array(
            "xmlns" => "urn:ebay:apis:eBLBaseComponents"
        ),
        "RequesterCredentials" => array(
            "eBayAuthToken" => $this->token(),
        ),
        "Action" => "Delete",
        "StoreCategories" => array(),
        "DestinationParentCategoryID" => ($parent_id) ? $parent_id : "-999",
    );

    foreach ($categories as $category) {
      $this->xml->data['StoreCategories']['CustomCategory'][] = $category;
    }

    $this->xml_request = $this->xml->getXML($this->callname);

    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    $result = $this->runService();

    if ($result['Ack'] == 'Success') {
      return TRUE;
    }

    else {
      $this->logResultError($result);
      return FALSE;
    }

  }

  /**
   *
   * @description
   *  This function moves store categories
   *  http://developer.ebay.com/devzone/xml/docs/Reference/ebay/SetStoreCategories.html
   *  !Note that this isn't being utilised in the current version but it kept here in
   *        case of future additions or in case the class is used elsewhere.
   *
   *  @return mixed: XML string if test run or boolean on success/failure
   *
   */
  function moveStoreCategories($categories, $parent_id=NULL) {
    $this->tokenActive = False;
    $this->tokenExpiry = 0;

    $this->callname = 'SetStoreCategories';
    $this->xml->data = array(
        "@attributes" => array(
            "xmlns" => "urn:ebay:apis:eBLBaseComponents"
        ),
        "RequesterCredentials" => array(
            "eBayAuthToken" => $this->token(),
        ),
        "Action" => "Move",
        "StoreCategories" => array(),
        "DestinationParentCategoryID" => ($parent_id) ? $parent_id : "-999",
    );

    foreach ($categories as $category) {
      $this->xml->data['StoreCategories']['CustomCategory'][] = $category;
    }

    $this->xml_request = $this->xml->getXML($this->callname);

    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    $result = $this->runService();

    if ($result['Ack'] == 'Success') {
      return TRUE;
    }

    else {
      $this->logResultError($result);
      return FALSE;
    }

  }

  /**
   *
   * @description
   *  This function renames store categories
   *  http://developer.ebay.com/devzone/xml/docs/Reference/ebay/SetStoreCategories.html
   *  !Note that this isn't being utilised in the current version but it kept here in
   *        case of future additions or in case the class is used elsewhere.
   *
   *  @return mixed: XML string if test run or boolean on success/failure
   *
   */
  function renameStoreCategories($categories, $parent_id=NULL) {
    $this->tokenActive = False;
    $this->tokenExpiry = 0;

    $this->callname = 'SetStoreCategories';
    $this->xml->data = array(
        "@attributes" => array(
            "xmlns" => "urn:ebay:apis:eBLBaseComponents"
        ),
        "RequesterCredentials" => array(
            "eBayAuthToken" => $this->token(),
        ),
        "Action" => "Rename",
        "StoreCategories" => array(),
        "DestinationParentCategoryID" => ($parent_id) ? $parent_id : "-999",
    );

    foreach ($categories as $category) {
      $this->xml->data['StoreCategories']['CustomCategory'][] = $category;
    }

    $this->xml_request = $this->xml->getXML($this->callname);

    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    $result = $this->runService();

    if ($result['Ack'] == 'Success') {
      return TRUE;
    }

    else {
      $this->logResultError($result);
      return FALSE;
    }

  }

  /**
   *
   * @description
   *  This function uploads all of the store categories using the SetStoreCategories API call:
   *  http://developer.ebay.com/devzone/xml/docs/Reference/ebay/SetStoreCategories.html
   *
   *  This function can only be called once to upload initial categories if none currently exist,
   *  it is also recursive, as only one level can be uploaded at a time, so to upload child categories,
   *  the parent categories need to be mapped first, then the children parsed and uploaded to those newly,
   *  mapped category ids.
   *
   *  !Note that this isn't being utilised in the current version but it kept here in
   *        case of future additions or in case the class is used elsewhere.
   *
   *  @return mixed|null: XML string if test run, FALSE on failure or result category on success
   *
   */

  function uploadStoreCategories($categories, $parent_id=NULL) {
    $this->tokenActive = False;
    $this->tokenExpiry = 0;

    $this->callname = 'SetStoreCategories';
    $this->xml->data = array(
        "@attributes" => array(
            "xmlns" => "urn:ebay:apis:eBLBaseComponents"
        ),
        "RequesterCredentials" => array(
            "eBayAuthToken" => $this->token(),
        ),
        "Action" => "Add",
        "StoreCategories" => array(),
        "DestinationParentCategoryID" => ($parent_id) ? $parent_id : "-999",
    );

    foreach ($categories as $category) {
      $this->xml->data['StoreCategories']['CustomCategory'][] = $category;
    }

    $this->xml_request = $this->xml->getXML($this->callname);

    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    $result = $this->runService();

    if ($result['Ack'] == 'Success') {
      return $result['CustomCategory']['CustomCategory'];
    }

    else {
      $this->logResultError($result);
      return NULL;
    }

  }

  /**
   *
   * @description
   *  This function gets all of the configuration for the eBay store, including its categories:
   *  http://developer.ebay.com/devzone/xml/docs/reference/ebay/GetStore.html
   *
   * @return mixed|null: XML string on test run, store result on success or NULL on failure
   *
   */
  function getStore($categories_only=FALSE) {
    $this->callname = 'GetStore';
    $this->xml->data = array(
        "@attributes" => array(
            "xmlns" => "urn:ebay:apis:eBLBaseComponents"
        ),
        "RequesterCredentials" => array(
            "eBayAuthToken" => $this->token(),
        ),
        "CategoryStructureOnly" => ($categories_only) ? 'true' : 'false',
    );

    $this->xml_request = $this->xml->getXML($this->callname);

    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    $result = $this->runService();

    if ($result['Ack'] == 'Success') {
      return $result['Store'];
    }

    else {
      $this->logResultError($result);
      return NULL;
    }

  }

  /**
   *
   * @description
   *  This function gets the categories listed on the ebay site, API reference:
   *  http://developer.ebay.com/Devzone/XML/docs/Reference/ebay/GetCategories.html
   *  !Note that this isn't being utilised in the current version but it kept here in
   *        case of future additions.
   *
   * @return mixed|null: XML string on test run, category result on success, NULL on failure
   *
   */
  function getCategories() {
    $this->callname = 'GetCategories';
    $this->xml->data = array(
      "@attributes" => array(
          "xmlns" => "urn:ebay:apis:eBLBaseComponents"
      ),
      "RequesterCredentials" => array(
          "eBayAuthToken" => $this->token(),
      ),
      "ViewAllNodes" => "true",
      "DetailLevel" => "ReturnAll",
    );


    $this->xml_request = $this->xml->getXML($this->callname);

    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    $result = $this->runService();

    if ($result['Ack'] == 'Success' && isset($result['CategoryArray'])) {
      return $result['CategoryArray']['Category'];
    }

    else {
      $this->logResultError($result);
      return NULL;
    }

  }

  /**
   *
   * @description
   *  This function sets a new session ID, from the following API call:
   *  http://developer.ebay.com/devzone/xml/docs/Reference/ebay/GetSessionID.html
   *
   * @return string: the XML feed if performing a test run, or a session ID
   *
   */
  private function getSessionID() {
    $this->callname = 'GetSessionID';

    $this->xml->data = array(
        "@attributes" => array(
            "xmlns" => "urn:ebay:apis:eBLBaseComponents"
        ),
        "RequesterCredentials" => array(
          "eBayAuthToken" => $this->token(),
        ),
        "RuName" => $this->ruName(),
        "Version" => $this->version,
        "WarningLevel" => "High",
    );

    $this->xml_request = $this->xml->getXML($this->callname);

    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    $result = $this->runService();

    if ($result['Ack'] == 'Success') {
      $this->sessionID = $result['SessionID'];
    }

    else {
      $this->logResultError($result);
      $this->sessionID = NULL;
    }

    return $this->sessionID;
  }

  /**
   *
   * @description
   *  This function submits a new item:
   *  http://developer.ebay.com/Devzone/XML/docs/Reference/ebay/AddFixedPriceItem.html
   * @param $nid: node id of the display item
   *
   * @return mixed|null
   *
   */
  public function addFixedPriceItem($nid) {
    if ($nid) {
      $this->callname = 'AddFixedPriceItem';

      $this->xml->data = array(
        "@attributes" => array(
          "xmlns" => "urn:ebay:apis:eBLBaseComponents"
        ),
        "RequesterCredentials" => array(
          "eBayAuthToken" => $this->token(),
        ),
        "Version" => $this->version,
        "WarningLevel" => "High",
      );

      $mapping = new CommerceEbayMapping($nid);
      $transposition = $mapping->transpose("Add Fixed Price Item");

      // Return NULL if there are no variations
      if (empty($transposition['Variations'])) {
        watchdog('Ebay_Trading', 'Item %node% upload skipped; no variations found',
          array('%node%' => $nid), WATCHDOG_INFO);
        return NULL;
      }

      // Preprare an array to prune out any variations with no quantities as these will return an error,
      // this includes their Pictures, and SpecificsSets
      $remove_variations = array();
      foreach ($transposition['Variations']['Variation'] as $idx => $variation) {
        if (!isset($variation['Quantity']) || (int) $variation['Quantity'] <= 0) {
          $remove_variations[$idx] = $transposition['Variations']['Variation'][$idx];
          watchdog('Ebay_Trading', 'Item %node% revision skipped; stock level beneath 0',
            array('%node%' => $nid), WATCHDOG_INFO);
        }
        // there might be quantity, but no specifics, so we remove the variation if so
        elseif (empty($variation['VariationSpecifics']) || !count($variation['VariationSpecifics'])) {
          $remove_variations[$idx] = $transposition['Variations']['Variation'][$idx];
          watchdog('Ebay_Trading', 'Item %node% revision skipped; missing variation specifics',
            array('%node%' => $nid), WATCHDOG_INFO);
        }
      }

      /**
       * Remove Any Variations that have been marked for removal, including Pictures and VariationSpecificsSet
       */
      foreach ($remove_variations as $variation_idx => $variation) {
        // prune variation pictures
        if (isset($transposition['Variations']['Pictures']['VariationSpecificPictureSet'])
          && isset($variation['VariationSpecifics'])) {
            $break = FALSE;
            foreach ($transposition['Variations']['Pictures']['VariationSpecificPictureSet']
                     as $picture_idx => $pictures) {
              if ($break) {
                break;
              }
              // unset pictures
              foreach ($variation['VariationSpecifics']['NameValueList'] as $nameValueList) {
                if ($nameValueList['Name'] == $transposition['Variations']['Pictures']['VariationSpecificName'] &&
                  $nameValueList['Value'] == $pictures['VariationSpecificValue']
                ) {
                  unset($transposition['Variations']['Pictures']['VariationSpecificPictureSet'][$picture_idx]);
                  $break = TRUE;
                  break;
                }
              }
            }
        }

        // prune specifics set
        if (isset($transposition['Variations']['VariationSpecificsSet']) && isset($variation['VariationSpecifics'])) {
          $break = FALSE;
          foreach ($transposition['Variations']['VariationSpecificsSet']['NameValueList'] as $set_idx => $set) {
            if ($break) {
              break;
            }
            foreach ($variation['VariationSpecifics']['NameValueList'] as $variationNameValueList) {
              if ($variationNameValueList['Name'] == $set['Name']) {
                foreach ($set['Value'] as $set_value_idx => $set_value) {
                  if ($set_value == $variationNameValueList['Value']) {
                    unset($transposition['Variations']['VariationSpecificsSet']['NameValueList'][$set_idx]
                      ['Value'][$set_value_idx]);
                  }
                }
                break;
              }
            }
          }
        }
        unset($transposition['Variations']['Variation'][$variation_idx]);
      }

      // Return NULL if there are no Variations for the item
      if (count($transposition['Variations']) <= 0) {
        watchdog('Ebay_Trading', 'Item %node% upload skipped; no stock or variation specifics set',
          array('%node%' => $nid), WATCHDOG_INFO);
        return NULL;
      }

      $this->xml->data['Item'] = $transposition;

      // Convert to XML and run the service
      $this->xml_request = $this->xml->getXML($this->callname);

      // if this is a test run, return the xml
      if ($this->test_run) {
        return $this->xml_request;
      }

      $result = $this->runService();
      if ($result['Ack'] == 'Success') {
        return $result;
      }

      if ($result['Ack'] == 'Warning') {
        $this->logResultError($result, WATCHDOG_WARNING);
        return $result;
      }

      else {
        $this->logResultError($result);
        return NULL;
      }

    }

    return NULL;
  }

  /**
   *
   * @description
   *  deletes an item from the store using the following API Call:
   *  http://developer.ebay.com/devzone/xml/docs/reference/ebay/endfixedpriceitem.html
   *
   * @param $SKU
   * @param string $EndReasonCodeType
   *  http://developer.ebay.com/devzone/xml/docs/reference/ebay/types/EndReasonCodeType.html
   *
   * @return mixed|null
   *
   */
  public function endFixedPriceItem($SKU, $EndReasonCodeType = "NotAvailable") {
    $this->callname = 'EndFixedPriceItem';
    $this->xml->data = array(
        "@attributes" => array(
            "xmlns" => "urn:ebay:apis:eBLBaseComponents"
        ),
        "RequesterCredentials" => array(
            "eBayAuthToken" => $this->token(),
        ),
        "Version" => $this->version,
        "WarningLevel" => "High",
    );

    $this->xml->data['EndingReason'] = $EndReasonCodeType;

    $tradingService = new CommerceEbayTradingService();
    $listing = $tradingService->storeHasListing($SKU);

    if (isset($listing['ItemArray']['Item']['ItemID'])) {
      $this->xml->data['ItemID'] = $listing['ItemArray']['Item']['ItemID'];
    }

    elseif (isset($listing['ItemArray']['Item']) && is_array($listing['ItemArray']['Item'])) {
      $item = end($listing['ItemArray']['Item']);
      $this->xml->data['ItemID'] = $item['ItemID'];
    }

    // if this is a test run but the item isn't listed, return the xml instead of erroring
    elseif ($this->test_run) {
      return $this->xml->getXML($this->callname);
    }

    else {
      watchdog('Ebay_Trading', 'Item with sku: %sku is not listed on eBay, cannot delist',
        array('%sku' => $SKU), WATCHDOG_WARNING);
      return NULL;
    }

    // Convert to XML and run the service
    $this->xml_request = $this->xml->getXML($this->callname);

    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    $result = $this->runService();
    if ($result['Ack'] == 'Success') {
      return $result;
    }

    if ($result['Ack'] == 'Warning') {
      $this->logResultError($result, WATCHDOG_WARNING);
      return $result;
    }

    else {
      $this->logResultError($result);
      return NULL;
    }

  }

  /**
   *
   * @description
   *  This function updates an item and is virtually identical to adding an item:
   *  http://developer.ebay.com/Devzone/XML/docs/Reference/ebay/ReviseItem.html
   *
   * @param $nid the node id of the display item
   *
   * @return mixed|null
   *
   */
  public function reviseFixedPriceItem($nid) {
    if ($nid) {
      // PRE-CHECK; make sure the item has a sku mapped and actually has been listed

      $mapping = new CommerceEbayMapping($nid);
      $transposition = $mapping->transpose("Revise Fixed Price Item");

      $result = $this->getItem($transposition['SKU']);
      if (isset($result['Item']['ListingDetails']['EndingReason'])) {
        watchdog($this->service_name, 'Skipping item %s, not listed on eBay',
          array('%s' => $transposition['SKU']), WATCHDOG_INFO);
        return FALSE;
      }

      $this->callname = 'ReviseFixedPriceItem';
      $this->xml->data = array(
          "@attributes" => array(
              "xmlns" => "urn:ebay:apis:eBLBaseComponents"
          ),
          "RequesterCredentials" => array(
              "eBayAuthToken" => $this->token(),
          ),
          "Version" => $this->version,
          "WarningLevel" => "High",
      );

      // Preprare an array to prune out any variations with no quantities as these will return an error,
      // this includes their Pictures, and SpecificsSets
      $zero_quantity = 0;
      foreach ($transposition['Variations']['Variation'] as $idx => $variation) {

        // Quantity must be 0 if less than or equal to update ebay stock
        if (!isset($variation['Quantity']) || (int) $variation['Quantity'] <= 0) {
          $transposition['Variations']['Variation'][$idx]['Quantity'] = 0;
          $zero_quantity++;
        }

      }

      // End the item instead if no variations have any stock
      if ($zero_quantity == count($transposition['Variations']['Variation'])) {
        return $this->endFixedPriceItem($transposition['SKU']);
      }

      $this->xml->data['Item'] = $transposition;


      // Convert to XML and run the service
      $this->xml_request = $this->xml->getXML($this->callname);

      // if this is a test run, return the xml
      if ($this->test_run) {
        return $this->xml_request;
      }

      $result = $this->runService();
      if ($result['Ack'] == 'Success') {
        return $result;
      }

      if ($result['Ack'] == 'Warning') {
        $this->logResultError($result, WATCHDOG_WARNING);
        return $result;
      }

      else {
        $this->logResultError($result);
        return NULL;
      }

    }

    return NULL;
  }

  /**
   *
   * @description
   *  This function gets all of the items currently listed on eBay.
   *  http://developer.ebay.com/devzone/xml/docs/Reference/ebay/GetSellerList.html
   *
   * @param Array $skus
   *
   * @return mixed|null|string
   *
   */
  public function getSellerList($skus=array()) {
    $this->callname = 'GetSellerList';

    $this->xml->data = array(
        "@attributes" => array(
            "xmlns" => "urn:ebay:apis:eBLBaseComponents"
        ),
        "RequesterCredentials" => array(
            "eBayAuthToken" => $this->token(),
        ),
        "Version" => $this->version,
        "WarningLevel" => "High",
    );

    $date = new DateTime();
    $this->xml->data['IncludeVariations'] = 'true';
    $this->xml->data['DetailLevel'] = 'ReturnAll';
    $this->xml->data['Pagination'] = array(
      'EntriesPerPage' => 20,
      'PageNumber' => 1,
    );
    $this->xml->data['StartTimeFrom'] = date('c', strtotime('-60 days', time()));
    $this->xml->data['StartTimeTo'] = $date->format("c");

    if ($skus) {
      foreach ($skus as $sku) {
        $this->xml->data['SKUArray'][]['SKU'] = $sku;
      }
    }

    $this->xml_request = $this->xml->getXML($this->callname);

    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    $result = $this->runService();
    if ($result['Ack'] == 'Success') {
      return $result;
    }

    else {
      $this->logResultError($result);
      return NULL;
    }

  }

  /**
   *
   * @description
   *  This function uses the GetSellerTransactions API call to get
   *  completed eBay transactions.
   *  http://developer.ebay.com/devzone/xml/docs/reference/ebay/GetSellerTransactions.html
   *
   * @param $modTimeFrom
   * @param $modTimeTo
   *
   * @return mixed|null
   *
   */
  public function getSellerTransactions($modTimeFrom, $modTimeTo) {
    $this->callname = 'GetSellerTransactions';

    $this->xml->data = array(
      "@attributes" => array(
        "xmlns" => "urn:ebay:apis:eBLBaseComponents"
      ),
      "RequesterCredentials" => array(
        "eBayAuthToken" => $this->token(),
      ),
      "Version" => $this->version,
      "WarningLevel" => "High",
    );

    $this->xml->data['ModTimeFrom'] = $modTimeFrom;
    $this->xml->data['ModTimeTo'] = $modTimeTo;

    $this->xml_request = $this->xml->getXML($this->callname);

    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    $result = $this->runService();

    if ($result['Ack'] == 'Success') {
      return $result;
    }

    else {
      $this->logResultError($result);
      return NULL;
    }

  }

  /**
   *
   * @description
   *  This function uses the GetSellerEvents API call to get information about
   *  products sold etc.
   *  http://developer.ebay.com/devzone/xml/docs/reference/ebay/GetSellerEvents.html
   *
   * @param $modTimeFrom
   * @param $modTimeTo
   *
   * @return mixed|null
   *
   */
  public function getSellerEvents($modTimeFrom, $modTimeTo) {
    $this->callname = 'GetSellerEvents';

    $this->xml->data = array(
      "@attributes" => array(
        "xmlns" => "urn:ebay:apis:eBLBaseComponents"
      ),
      "RequesterCredentials" => array(
        "eBayAuthToken" => $this->token(),
      ),
      "Version" => $this->version,
      "WarningLevel" => "High",
    );

    $this->xml->data['modTimeFrom'] = $modTimeFrom;
    $this->xml->data['modTimeTo'] = $modTimeTo;

    $this->xml_request = $this->xml->getXML($this->callname);
    $result = $this->runService();

    if ($result['Ack'] == 'Success') {
      return $result;
    }

    else {
      $this->logResultError($result);
      return NULL;
    }

  }

  /**
   *
   * @description
   *  This function uses the same getSellerList function except is used to just
   *  check if an item actually exists in store.
   *  http://developer.ebay.com/devzone/xml/docs/Reference/ebay/GetSellerList.html
   *
   * @param $sku
   *
   * @return mixed|null
   *
   */
  public function storeHasListing($sku=NULL) {
    $this->callname = 'GetSellerList';

    $this->xml->data = array(
        "@attributes" => array(
            "xmlns" => "urn:ebay:apis:eBLBaseComponents"
        ),
        "RequesterCredentials" => array(
            "eBayAuthToken" => $this->token(),
        ),
        "Version" => $this->version,
        "WarningLevel" => "High",
    );

    $date = new DateTime();
    $this->xml->data['IncludeVariations'] = 'true';
    $this->xml->data['StartTimeFrom'] = date('c', strtotime('-60 days', time()));
    $this->xml->data['StartTimeTo'] = $date->format("c");
    $this->xml->data['SKUArray'][]['SKU'] = $sku;

    $this->xml_request = $this->xml->getXML($this->callname);

    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    $result = $this->runService();

    if ($result['Ack'] == 'Success') {
      return $result;
    }

    else {
      $this->logResultError($result);
      return NULL;
    }

  }


  /**
   *
   * @description
   *  gets an item from the ebay store by sku
   *  http://developer.ebay.com/devzone/xml/docs/reference/ebay/getitem.html
   *
   * @param $sku
   *
   * @return mixed|null
   *
   */
  public function getItem($sku=NULL) {
    $this->callname = 'GetItem';

    $this->xml->data = array(
      "@attributes" => array(
        "xmlns" => "urn:ebay:apis:eBLBaseComponents"
      ),
      "RequesterCredentials" => array(
        "eBayAuthToken" => $this->token(),
      ),
      "Version" => $this->version,
      "WarningLevel" => "High",
    );

    $this->xml->data['SKU'] = $sku;

    $this->xml_request = $this->xml->getXML($this->callname);

    // if this is a test run, return the xml
    if ($this->test_run) {
      return $this->xml_request;
    }

    $result = $this->runService();

    if ($result['Ack'] == 'Success') {
      return $result;
    }

    else {
      $this->logResultError($result);
      return NULL;
    }

  }



}
