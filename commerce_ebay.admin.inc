<?php
/**
 * @file commerce_ebay.admin.inc
 */

/**
 *
 * @description
 *   an admin settings form for the module
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 *
 */
function commerce_ebay_admin_form($form, &$form_state) {

  form_load_include($form_state, 'inc', 'commerce_ebay', 'includes/commerce_ebay.tasks');

  $form['ebay-settings'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Ebay Settings'),
  );

  $form['ebay-settings']['ebay-sandbox'] = array(
      '#type'           => 'checkbox',
      '#title'          => t('Sandbox Mode'),
      '#description'    => t('Run the application in sandbox mode'),
      '#default_value'  => variable_get('ebay-sandbox', 0),
  );

  $form['ebay-settings']['ebay-enable-cron'] = array(
      '#type'           => 'checkbox',
      '#title'          => t('Enable Cron'),
      '#default_value'  => variable_get('ebay-enable-cron'),
  );

  $form['ebay-settings']['ebay-queue-limit'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Queue Limit'),
    '#description'    => t('The amount of items that should be added/revised/removed during one cron run'),
    '#default_value'  => variable_get('ebay-queue-limit', 100),
  );

  $form['ebay-settings']['ebay-host-restrict'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Restrict Cron To Hostname'),
      '#description'    => t('Restrict module use to run cron on the given host name, useful when running a staging and production environment. Note this uses the gethostname() php function, not the HTTP_HOST'),
      '#default_value'  => variable_get('ebay-host-restrict', ''),
  );

  if (gethostname() == variable_get('ebay-host-restrict', '')) {
    $form['ebay-settings']['ebay-host-restrict']['#attributes'] = array('style' => array('border: 2px solid lime;'));
  }
  else {
    $form['ebay-settings']['ebay-host-restrict']['#attributes'] = array('style' => array('border: 2px solid red;'));
  }

  $form['ebay-settings']['ebay-config-filename'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Config Filename'),
    '#description'    => t('The yaml config filename; it must be placed in the includes directory relative to the module'),
    '#default_value'  => variable_get('ebay-config-filename', 'commerce_ebay.map.yaml'),
  );

  foreach (taxonomy_get_vocabularies() as $vocabulary) {
    $form['ebay-settings']['ebay-store-categories']['#options'][$vocabulary->vid] = $vocabulary->machine_name;
  }

  $form['ebay-settings']['sandbox'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Sandbox Settings'),
    '#collapsible'  => TRUE,
    '#collapsed'    => TRUE,
  );

  $form['ebay-settings']['sandbox']['ebay-sandbox-devid'] = array(
      '#type'         => 'textfield',
      '#title'        => t('Dev ID'),
      '#value'        => variable_get('ebay-sandbox-devid', ''),
  );

  $form['ebay-settings']['sandbox']['ebay-sandbox-appid'] = array(
      '#type'         => 'textfield',
      '#title'        => t('App ID'),
      '#value'        => variable_get('ebay-sandbox-appid', ''),
  );

  $form['ebay-settings']['sandbox']['ebay-sandbox-certid'] = array(
      '#type'         => 'textfield',
      '#title'        => t('Cert ID'),
      '#value'        => variable_get('ebay-sandbox-certid', ''),
  );

  $form['ebay-settings']['sandbox']['ebay-sandbox-token'] = array(
      '#type'         => 'textarea',
      '#title'        => t('Token'),
      '#description'  => t('A User API Token'),
      '#value'        => variable_get('ebay-sandbox-token', ''),
  );

  $form['ebay-settings']['sandbox']['ebay-sandbox-runame'] = array(
      '#type'         => 'textfield',
      '#title'        => t('RuName'),
      '#description'  => t('A generated RuName for the application.'),
      '#value'        => variable_get('ebay-sandbox-runame', ''),
  );

  $form['ebay-settings']['sandbox']['ebay-sandbox-siteid'] = array(
      '#type'         => 'select',
      '#title'        => t('Site ID'),
      '#description'  => t('The eBay site for the store.'),
      '#value'        => variable_get('ebay-sandbox-siteid', 0),
      '#options'      => _commerce_ebay_siteid_options(),
  );

  $form['ebay-settings']['production'] = array(
      '#type'         => 'fieldset',
      '#title'        => t('Production Settings'),
      '#collapsible'  => TRUE,
      '#collapsed'    => TRUE,
  );

  $form['ebay-settings']['production']['ebay-production-devid'] = array(
      '#type'         => 'textfield',
      '#title'        => t('Dev ID'),
      '#value'        => variable_get('ebay-production-devid', ''),
  );

  $form['ebay-settings']['production']['ebay-production-appid'] = array(
      '#type'         => 'textfield',
      '#title'        => t('App ID'),
      '#value'        => variable_get('ebay-production-appid', ''),
  );

  $form['ebay-settings']['production']['ebay-production-certid'] = array(
      '#type'         => 'textfield',
      '#title'        => t('Cert ID'),
      '#value'        => variable_get('ebay-production-certid', ''),
  );

  $form['ebay-settings']['production']['ebay-production-token'] = array(
      '#type'         => 'textarea',
      '#title'        => t('Token'),
      '#description'  => t('A User API Token'),
      '#value'        => variable_get('ebay-production-token', ''),
  );

  $form['ebay-settings']['production']['ebay-production-runame'] = array(
      '#type'         => 'textfield',
      '#title'        => t('RuName'),
      '#description'  => t('A generated RuName for the application.'),
      '#value'        => variable_get('ebay-production-runame', ''),
  );

  $form['ebay-settings']['production']['ebay-production-siteid'] = array(
      '#type'         => 'select',
      '#title'        => t('Site ID'),
      '#description'  => t('The eBay site for the store.'),
      '#value'        => variable_get('ebay-production-siteid', 0),
      '#options'      => _commerce_ebay_siteid_options(),
  );

  $form['ebay-settings']['sample-feed-generator'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Sample Feed Generator'),
    '#description'  => 'Generate a sample of XML from one of the available trading operations',
    '#collapsible'  => TRUE,
    '#collapsed'    => TRUE,
  );

  $form['ebay-settings']['sample-feed-generator']['feed-type'] = array(
    '#type'     => 'select',
    '#title'    => t('Feed Type'),
    '#suffix'   => '<div id="commerce-ebay-sample-feed-wrapper"></div><div id="commerce-ebay-sample-feed-results"></div>',
    '#empty_option' => t('-- Select Feed Type --'),
    '#options'  => array(
      'addFixedPriceItem' => 'Add Fixed Price Item',
      'endFixedPriceItem' => 'End Fixed Price Item',
      'reviseFixedPriceItem' => 'Revise Fixed Price Item',
      'getSellerList' => 'Get Seller List',
      'getSellerTransactions' => 'Get Seller Transactions',
    ),
    '#ajax' => array(
      'event'       => 'change',
      'wrapper'     => 'commerce-ebay-sample-feed-wrapper',
      'callback'    => '_commerce_ebay_admin_sample_feed_select_callback',
      'method'      => 'replace'
    )
  );

  $form['actions']['ebay-sync-all-terms'] = array(
    '#type'         => 'submit',
    '#value'        => 'Queue All Items',
    '#name'         => 'ebay-sync',
    '#weight'       => 10,
    '#submit'       => array('commerce_ebay_task_queue_all_items'),
  );

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'commerce_ebay') . '/css/commerce_ebay_admin.style.css'
  );

  return system_settings_form($form);
}

/**
 *
 * @description
 *  a submit handler for the admin form.
 *
 * @param $form
 * @param $form_state
 *
 */
function commerce_ebay_admin_form_submit($form, &$form_state) {
  $exclude = array('form_build_id', 'form_token', 'form_id', 'op');
  foreach ($form_state['input'] as $key => $val) {

    if (!in_array($key, $exclude)) {
      variable_set($key, $val);
    }

    // an array will be options; set each to original-key_options_val
    if (is_array($val)) {
      foreach ($val as $options_key => $options_val) {
        variable_set($key . '_' . $options_key, $options_val);
      }
    }
  }
}

/**
 *
 * @description
 *  create an options array of possible siteid options
 *  definied by ebay.
 *
 * @return array
 *
 */
function _commerce_ebay_siteid_options() {
  $options = array();
  foreach (_commerce_ebay_global_mapping() as $mapping) {
    $options[$mapping['Site ID']] = $mapping['Site Name'];
  }
  return $options;
}


/**
 *
 * @description
 *  callback to display the sample feed type arguments depending on which
 *  sample feed type is selected.
 * @param $form
 * @param $form_state
 *
 * @return string
 *
 */
function _commerce_ebay_admin_sample_feed_select_callback(&$form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $form = drupal_rebuild_form($form['#form_id'], $form_state, $form);

  if (isset($form['ebay-settings']['sample-feed-generator']['feed-type-arguments'])) {
    return $form['ebay-settings']['sample-feed-generator']['feed-type-arguments'];
  }
  else {
    return '<div id="commerce-ebay-sample-feed-wrapper"></div>';
  }
}


/**
 * callback function that executes the sample feed via a reflection method,
 * some arguments may be broken out into an array if required.
 * @param $form
 * @param $form_state
 * @return string
 */
function _commerce_ebay_admin_sample_feed_execute_callback($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  if ($feed_type = $form_state['values']['feed-type']) {
    $service = new CommerceEbayTradingService();
    // do a test run so we just get the xml feed that would be sent
    $service->test_run = TRUE;
    $params = array();

    if (method_exists('CommerceEbayTradingService', $feed_type)) {
      $method = new ReflectionMethod('CommerceEbayTradingService', $feed_type);
      foreach ($method->getParameters() as $arg) {
        // some parameters must be passed as an array "skus" is a parameter which
        // must be passed as an array or comma delimited list of strings.
        if (in_array($arg->name, array('skus'))) {
          $params[] = explode(',', $form_state['values'][$arg->name]);
        }
        else {
          $params[] = $form_state['values'][$arg->name];
        }
      }
    }
    $result = call_user_func_array(array($service, $feed_type), $params);
    return "<pre id='commerce-ebay-sample-feed-results' class='success'>" . htmlspecialchars($result) . "</pre>";
  }
  return "<pre id='commerce-ebay-sample-feed-results'>" . print_r($form_state['values'], TRUE) . "</pre>";
}
